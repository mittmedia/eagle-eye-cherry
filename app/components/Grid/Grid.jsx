import React from 'react';
import classNames from 'classnames';

import styles from './Grid.scss';

export default class Grid extends React.Component {
  render() {
    const classes = classNames(
      styles.grid,
      styles['small-grid'],
      styles['medium-grid'],
      styles['large-grid'],
      styles['margin-bottom-' + this.props.marginBottom],
      styles['margin-top-' + this.props.marginTop]
    );
    return (
      <div className={classes}>
        {this.props.children}
      </div>
    )
  }
}

Grid.propTypes = {
  marginBottom: React.PropTypes.string,
  marginTop: React.PropTypes.string
}
