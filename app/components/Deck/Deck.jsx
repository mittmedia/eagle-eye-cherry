import React from 'react';
require('es6-promise').polyfill();
import fetch from 'isomorphic-fetch';
import styles from './Deck.scss';
import Settings from './Settings';
import Feed from './Feed';

export default class Deck extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      articles: []
    };
  }

  render() {
    return (
      <div className={styles.deck}>
        <Settings />
        <Feed articles={this.state.articles} />
      </div>
    )
  }

  componentDidMount() {
    this.searchArticles(['sundsvall'], 5);
  }

  searchArticles = (labels, limit) => {
    const query = `${labels.join('|')}&limit=${limit}`;
    fetch('http://localhost:4000/search?q=' + query)
      .then((response) => {
        if (response.status >= 400) {
          throw new Error("Bad response from server");
        }
        return response.json();
      })
      .then((data) => {
        this.setState({
          articles: data.result
        });
      });
  }

}
