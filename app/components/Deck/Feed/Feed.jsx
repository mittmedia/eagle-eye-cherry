import React from 'react';
import styles from './Feed.scss';
import Article from './Article';

export default () => (
  <div className={styles.feed}>
    <Article title="Caw ca caw" text="She's not 'that Mexican', Mom. She's my Mexican. And she's Colombian or something. Did you know that more frozen bananas are sold right here on this boardwalk than anywhere on the OC? What have we always said is the most important thing? Interfere? I ought to pull down your pants and spank your ass raw. Michael: I'm sorry, have we met?" />
    <Article title="It's a jetpack" text="The Army had half a day. Say goodbye to THESE! If this were a Lifetime Moment of Truth movie, this would be our act break. But it wasn't. He's going to be all right. Stop licking my hand, you horse's ass." />
    <Article title="Buster" text="These are my awards, Mother. From Army. The seal is for marksmanship, and the gorilla is for sand racing. Yeah, like I'm going to spill coffee all over this $3,000 suit? Come on!" />
    <Article title="It's Sunday, but screw it — juice box time" text="You just grab that brownish area by its points and you don't let go no matter what your mother tells you! Do you have any idea how often you say the word afraid? Well, I know I used it in the Jacuzzi. I've got a nice hard cot with his name on it. You'd do that to your own brother? I said taste the happy, Michael. Taste it. It tastes kind of like sad." />
  </div>
);
