import './app.scss';

import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute } from 'react-router';
import AppContainer from './containers/AppContainer';
import HomePageContainer from './containers/HomePageContainer';
import AboutPageContainer from './containers/AboutPageContainer';
import createHashHistory from 'history/lib/createHashHistory'



let history = createHashHistory()

render((
  <Router history={history}>
    <Route path="/" component={AppContainer}>
      <IndexRoute component={HomePageContainer} />
      <Route path="about" component={AboutPageContainer} />
    </Route>
  </Router>
), document.getElementById('react-root'));
