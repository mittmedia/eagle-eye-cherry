import React from 'react';
import { RouteHandler } from 'react-router';
import styles from './AppContainer.scss';

export default class AppContainer extends React.Component {

  render() {
    return (
      <div className={styles.app}>
        {this.props.children}
      </div>
    );
  }

}

