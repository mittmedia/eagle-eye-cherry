import React from 'react';
import classNames from 'classnames';

import styles from './Grid.scss';

export default class Cell extends React.Component {
  render() {
    const classes = classNames(
      styles.cell,
      styles['small-cell'],
      styles['medium-cell'],
      styles['large-cell'],
      styles['small-cell-' + this.props.small],
      styles['medium-cell-' + this.props.medium],
      styles['large-cell-' + this.props.large],
      styles['margin-bottom-' + this.props.marginBottom],
      styles['margin-top-' + this.props.marginTop]
    )
    return (
      <div className={classes}>
        {this.props.children}
      </div>
    )
  }
}

Cell.propTypes = {
  marginBottom: React.PropTypes.string,
  marginTop: React.PropTypes.string
}

Cell.defaultProps = {
  large: 'full',
  medium: 'full',
  small: 'full'
}
