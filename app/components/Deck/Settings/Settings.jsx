import React from 'react';
import styles from './Settings.scss';

export default class Settings extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      keywords: ['foo', 'bar', 'tjeni']
    };
  }

  onKeyDown = (e) => {
    console.log(e);
    var key = e.keyCode;
    console.log(key);
    if (key === 13) {
      this.setState({
        keywords: this.state.keywords.concat([e.target.value])
      })
      e.target.value = '';
    }
  }

  render() {

    var keywords = this.state.keywords.map((word, i) => {
      i = 'key-' + i;
      return (
        <li className={styles.li} key={i}>
          {word}
        </li>
      );
    });

    return (
      <div className={styles.settings}>
        <ul className={styles.ul}>
          {keywords}
        </ul>
        <input className={styles.input} placeholder='Kategori...' onKeyDown={this.onKeyDown} />
      </div>
    );
  }
};
