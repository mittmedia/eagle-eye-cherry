import React from 'react';
import { Link } from 'react-router';
import { Grid, Cell } from '../components/Grid';
import Navigation from '../components/Navigation';
import Deck from '../components/Deck';
import styles from './HomePageContainer.scss';

export default class HomePageContainer extends React.Component {

  static defaultProps = {

  }

  render() {
    return (
      <div className={styles.container}>
        <Navigation />
        <div className={styles.grid}>
          <Deck />
          <Deck />
          <Deck />
        </div>
      </div>
    );
  }
}
